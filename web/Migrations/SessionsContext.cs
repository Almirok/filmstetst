﻿
using web.Models;
using System.Data.Entity;


namespace web.Migrations
{
    public class SessionsContext : DbContext
    {
        public SessionsContext() : base("DefaultConnection")
        {

        }

        public DbSet<FilmModel> Films { get; set; }
        
        protected override void OnModelCreating(DbModelBuilder modelBuilder)
        {

        }
    }
}