﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Web;
using System.Web.Hosting;
using System.Web.Mvc;
using web.Migrations;
using web.Models;
using PagedList;
using Microsoft.AspNet.Identity;

namespace web.Controllers
{
    public class HomeController : Controller
    {
        public static SessionsContext Repository { get; } = new SessionsContext();
        public static ApplicationDbContext Users { get; } = new ApplicationDbContext();
        public ActionResult Index(int? page)
        {
            var models = new List<FilmModel>();
            var entities = Repository.Films.ToList();
            int pageSize = 2;
            int pageNumber = (page ?? 1);
            foreach (var entity in entities)
            {
                models.Add(InitializeModel(entity));
            }
            return View(models.ToPagedList<FilmModel>(pageNumber, pageSize));
        }

        public ActionResult Create()
        {

            return View();
        }

        [HttpPost]
        public ActionResult Create(HttpPostedFileBase file, FilmModel model)
        {
            Random rnd = new Random();
            var user = User.Identity.GetUserId();
            if (user == null)
            {
                ModelState.AddModelError(string.Empty, "Необходимо авторизоваться");
                return View();
            }
            var namePoster = rnd.Next(0, 100000);
            var path = Path.Combine(HostingEnvironment.MapPath("~/Content/Posters/"), namePoster + ".jpg");
            file.SaveAs(path);

            var posterPath = "/Content/Posters/" + namePoster + ".jpg";
            FilmModel session = new FilmModel(model.Name, model.Description, model.YearOfIssue, model.Producer, posterPath, User.Identity.GetUserId());
            Repository.Films.Add(session);
            Repository.SaveChanges();

            return RedirectToAction("Index");
        }
        [ValidateAntiForgeryToken]
        public ActionResult Edit(int id)
        {
            var entity = Repository.Films.Find(id);

            if (entity != null)
            {
                var model = InitializeModel(entity);
                return View(model);
            }
            return RedirectToAction("Index");

        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit(HttpPostedFileBase file, FilmModel model)
        {
            var entity = Repository.Films.SingleOrDefault(m => m.Id == model.Id);

            Random rnd = new Random();
            var namePoster = rnd.Next(0, 100000);
            var path = Path.Combine(HostingEnvironment.MapPath("~/Content/Posters/"), namePoster + ".jpg");
            file.SaveAs(path);
            var posterPath = "/Content/Posters/" + namePoster + ".jpg";

            if (entity != null)
            {
                entity.Update(model.Name, model.Description, model.YearOfIssue, model.Producer, posterPath, entity.IPCreator);
                Repository.SaveChanges();
            }
            return RedirectToAction("Index");
        }


        [ValidateAntiForgeryToken]
        public ActionResult Delete(int id)
        {
            var entity = Repository.Films.Find(id);

            if (entity != null)
            {
                Repository.Films.Remove(entity);
                Repository.SaveChanges();
            }

            return RedirectToAction("Index");
        }

        private static FilmModel InitializeModel(FilmModel entity)
        {

            var model = new FilmModel
            {
                Id = entity.Id,
                Name = entity.Name,
                Description = entity.Description,
                YearOfIssue = entity.YearOfIssue,
                Producer = entity.Producer,
                Image = entity.Image,
                IPCreator = entity.IPCreator
            };
            return model;
        }

    }
}