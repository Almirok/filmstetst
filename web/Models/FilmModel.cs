﻿using System;


namespace web.Models
{
    public class FilmModel
    {
        //название, описание, год выпуска, режиссёр, постер, пользователь, который выложил информацию
        public FilmModel()
        {

        }
        public int Id { get; set; }
        public string Name { get; set; }
        public string Description { get; set; }
        public int YearOfIssue { get; set; }
        public string Producer { get; set; }
        public string Image { get; set; }
        public string IPCreator { get; set; }

        public FilmModel(string name, string description, int yearodissue, string producer, string image, string ipcreator)
        {
            Name = name;
            Description = description;
            YearOfIssue = yearodissue;
            Producer = producer;
            Image = image;
            IPCreator = ipcreator;
        }

        public FilmModel Update(string name, string description, int yearodissue, string producer, string image, string ipcreator)
        {
            Name = name;
            Description = description;
            YearOfIssue = yearodissue;
            Producer = producer;
            Image = image;
            IPCreator = ipcreator;
            return this;
        }
    }
}